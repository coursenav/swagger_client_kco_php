# Customer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_of_birth** | **string** | Customer’s date of birth. The format is ‘yyyy-mm-dd’. ISO 8601 date. | [optional] 
**type** | **string** | The default supported value is &#x27;person&#x27;. If B2B is enabled for the merchant, the value may be \&quot;organization\&quot;. | [optional] 
**organization_registration_id** | **string** | \&quot;The organization&#x27;s official registration id (organization number).  Note: Applicable only for B2B orders.\&quot; | [optional] 
**gender** | **string** | Customer’s gender - ‘male’ or ‘female’. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

