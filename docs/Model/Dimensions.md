# Dimensions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**height** | **int** | The product&#x27;s height as used in the merchant&#x27;s webshop. Non-negative. Measured in millimeters. | [optional] 
**width** | **int** | The product&#x27;s width as used in the merchant&#x27;s webshop. Non-negative. Measured in millimeters. | [optional] 
**length** | **int** | The product&#x27;s length as used in the merchant&#x27;s webshop. Non-negative. Measured in millimeters. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

