# RecurringOrderRequestV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**locale** | **string** |  | [optional] 
**cart** | [**\Swagger\Client\Checkout\Checkout\Model\AggregatedOrderCartV2**](AggregatedOrderCartV2.md) |  | [optional] 
**merchant** | [**\Swagger\Client\Checkout\Checkout\Model\MerchantAggregatedOrderV2**](MerchantAggregatedOrderV2.md) |  | 
**merchant_reference** | **map[string,string]** |  | [optional] 
**purchase_country** | **string** |  | [optional] 
**purchase_currency** | **string** |  | [optional] 
**billing_address** | [**\Swagger\Client\Checkout\Checkout\Model\AggregatedOrderAddressV2**](AggregatedOrderAddressV2.md) |  | [optional] 
**shipping_address** | [**\Swagger\Client\Checkout\Checkout\Model\AggregatedOrderAddressV2**](AggregatedOrderAddressV2.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

