# ShippingAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weight** | **int** | The product&#x27;s weight as used in the merchant&#x27;s webshop. Non-negative. Measured in grams. | [optional] 
**dimensions** | [**\Swagger\Client\Checkout\Checkout\Model\Dimensions**](Dimensions.md) |  | [optional] 
**tags** | **string[]** | The product&#x27;s extra features | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

