<?php
/**
 * MerchantAggregatedOrderRequestUpdateV2Test
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client\Checkout
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.20
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client\Checkout;

/**
 * MerchantAggregatedOrderRequestUpdateV2Test Class Doc Comment
 *
 * @category    Class
 * @description MerchantAggregatedOrderRequestUpdateV2
 * @package     Swagger\Client\Checkout
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class MerchantAggregatedOrderRequestUpdateV2Test extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "MerchantAggregatedOrderRequestUpdateV2"
     */
    public function testMerchantAggregatedOrderRequestUpdateV2()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "terms_uri"
     */
    public function testPropertyTermsUri()
    {
    }

    /**
     * Test attribute "checkout_uri"
     */
    public function testPropertyCheckoutUri()
    {
    }

    /**
     * Test attribute "confirmation_uri"
     */
    public function testPropertyConfirmationUri()
    {
    }

    /**
     * Test attribute "push_uri"
     */
    public function testPropertyPushUri()
    {
    }

    /**
     * Test attribute "validation_uri"
     */
    public function testPropertyValidationUri()
    {
    }

    /**
     * Test attribute "back_to_store_uri"
     */
    public function testPropertyBackToStoreUri()
    {
    }

    /**
     * Test attribute "cancellation_terms_uri"
     */
    public function testPropertyCancellationTermsUri()
    {
    }
}
